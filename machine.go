package main

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

// machine represents a physical machine, that is, a desktop or a laptop
// workstation.
type machine struct {
	MachineType string
	Disk        map[string]string
}

func NewMachine(a *Answers) *machine {
	m := machine{
		MachineType: "desktop",
		Disk: map[string]string{
			"targetDisk": "/dev/nvme0n1",
			"bootPart":   "/dev/nvme0n1p1",
			"rootPart":   "/dev/nvme0n1p2",
			"tmpPart":    "/dev/nvme0n1p3",
			"homePart":   "/dev/nvme0n1p4",
		},
	}

	if a.MachineType == "laptop" {
		m.MachineType = "laptop"
		m.Disk["targetDisk"] = "/dev/sdb"
		m.Disk["bootPart"] = "/dev/sdb1"
		m.Disk["rootPart"] = "/dev/sdb2"
		m.Disk["tmpPart"] = "/dev/sdb3"
		m.Disk["swapPart"] = "/dev/sdb4"
		m.Disk["homePart"] = "/dev/sdb5"
	}

	return &m
}

// wipeDisk wipes the target drive.
func (m *machine) wipeDisk(ctx context.Context) error {
	if err := Command(
		"sgdisk",
		"--zap-all",
		m.Disk["targetDisk"],
	).Run(ctx); err != nil {
		return err
	}

	// Wipe any leftover filesystem metadata.
	return Command(
		"wipefs",
		"--all",
		m.Disk["targetDisk"],
	).Run(ctx)

}

// partitionDisk partitions the target drive.
func (m *machine) partitionDisk(ctx context.Context) error {
	if err := Command(
		"sgdisk",
		"-og",
		m.Disk["targetDisk"],
	).Run(ctx); err != nil {
		return err
	}

	// Create an EFI partition.
	if err := Command(
		"sgdisk",
		"--new=0:0:+600MiB",
		"--change-name=0:/boot",
		"--typecode=0:ef00",
		m.Disk["targetDisk"],
	).Run(ctx); err != nil {
		return err
	}

	// Create a /root partition.
	if err := Command(
		"sgdisk",
		"--new=0:0:+35GiB",
		"--change-name=0:/root",
		"--typecode=0:8304",
		m.Disk["targetDisk"],
	).Run(ctx); err != nil {
		return err
	}

	// Create a /tmp partition.
	if err := Command(
		"sgdisk",
		"--new=0:0:+8GiB",
		"--change-name=0:/tmp",
		"--typecode=0:8300",
		m.Disk["targetDisk"],
	).Run(ctx); err != nil {
		return err
	}

	if m.MachineType == "laptop" {
		// Create a swap partition.
		if err := Command(
			"sgdisk",
			"--new=0:0:+4GiB",
			"--change-name=0:[SWAP]",
			"--typecode=0:8200",
			m.Disk["targetDisk"],
		).Run(ctx); err != nil {
			return err
		}
	}

	// Create a /home partition with a relative negative end.
	offset, err := offset(m)
	if err != nil {
		return err
	}

	homePart := fmt.Sprintf("--new=0:0:-%sGiB", offset)
	if err := Command(
		"sgdisk",
		homePart,
		"--change-name=0:/home",
		"--typecode=0:8302",
		m.Disk["targetDisk"],
	).Run(ctx); err != nil {
		return err
	}

	return nil
}

func offset(m *machine) (string, error) {
	var b bytes.Buffer

	cmd := Command(
		"lsblk",
		"--output",
		"SIZE",
		"-n",
		"-d",
		m.Disk["targetDisk"],
	)
	cmd.Stdout = &b
	if err := cmd.Run(context.Background()); err != nil {
		return "", errors.Wrap(err, "computing disk offset")
	}

	s := b.String()
	if strings.Contains(s, ".") {
		str := strings.Split(s, ".")[0]
		i, _ := strconv.Atoi(str)
		i = (i * 10) / 100
		return strconv.Itoa(i), nil
	}

	i, _ := strconv.Atoi(s)
	i = (i * 10) / 100
	return strconv.Itoa(i), nil
}

// formatPartitions formats the partitions.
func (m *machine) formatPartitions(ctx context.Context) error {
	if err := Command(
		"mkfs.fat",
		"-F32",
		m.Disk["bootPart"],
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"mkfs.ext4",
		m.Disk["rootPart"],
		"-F",
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"mkfs.ext4",
		m.Disk["tmpPart"],
		"-F",
	).Run(ctx); err != nil {
		return err
	}

	if m.MachineType == "laptop" {
		if err := Command(
			"mkswap",
			m.Disk["swapPart"],
		).Run(ctx); err != nil {
			return err
		}
	}

	if err := Command(
		"mkfs.ext4",
		m.Disk["homePart"],
		"-F",
	).Run(ctx); err != nil {
		return err
	}

	return nil
}

func (m *machine) mountPartitions(ctx context.Context) error {
	if err := Command(
		"mount",
		m.Disk["rootPart"],
		"/mnt",
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"mkdir",
		"-p",
		"/mnt/boot",
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"mkdir",
		"-p",
		"/mnt/tmp",
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"mkdir",
		"-p",
		"/mnt/home",
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"mount",
		m.Disk["bootPart"],
		"/mnt/boot",
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"mount",
		m.Disk["tmpPart"],
		"/mnt/tmp",
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"mount",
		m.Disk["homePart"],
		"/mnt/home",
	).Run(ctx); err != nil {
		return err
	}

	if m.MachineType == "laptop" {
		if err := Command(
			"swapon",
			m.Disk["swapPart"],
		).Run(ctx); err != nil {
			return err
		}
	}

	return nil
}

func (m *machine) setRootUUID(ctx context.Context) error {
	var s bytes.Buffer

	cmd := Command(
		"lsblk",
		"-dno",
		"UUID",
		m.Disk["rootPart"],
	)
	cmd.RedirectStdout(&s)
	if err := cmd.Run(ctx); err != nil {
		return err
	}

	s.Truncate(36)

	if err := os.Setenv("ROOT_UUID", fmt.Sprintf("options root=UUID=%s rw", s.String())); err != nil {
		return err
	}

	return nil
}
