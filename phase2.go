package main

import (
	"context"
	"github.com/pkg/errors"
	"log"
)

func phase2(ctx context.Context, log *log.Logger) error {
	log.Println("Configuring UEFI bootloader")
	if err := bootloaderConfig(ctx); err != nil {
		return errors.Wrap(err, "configuring bootloader")
	}

	log.Println("Generating locale")
	if err := genLocale(ctx); err != nil {
		return errors.Wrap(err, "generating locale")
	}

	log.Println("Setting local time")
	if err := setLocalTime(ctx); err != nil {
		return errors.Wrap(err, "setting local time")
	}

	log.Println("Setting hardware clock")
	if err := setHWClock(ctx); err != nil {
		return errors.Wrap(err, "setting hardware clock")
	}

	log.Println("Setting keymap")
	if err := setKeyMap(); err != nil {
		return errors.Wrap(err, "setting keymap")
	}

	log.Println("Configuring networking")
	if err := networkConfig(); err != nil {
		return errors.Wrap(err, "configuring network")
	}

	log.Println("Setting root password")
	if err := setPassword(ctx, "root", rootPw); err != nil {
		return errors.Wrap(err, "setting root password")
	}

	log.Println("Installing essential packages")
	if err := installPkgs(ctx); err != nil {
		return errors.Wrap(err, "installing essential packages")
	}

	log.Println("Updating sudoers file")
	if err := updateSudoers(); err != nil {
		return errors.Wrap(err, "updating sudoers file")
	}

	log.Printf("Creating user %s with default shell %s", userName, zshPath)
	if err := createUser(ctx, zshPath); err != nil {
		return errors.Wrapf(err, "creating user: %s", userName)
	}

	log.Printf("Setting password for %s", userName)
	if err := setPassword(ctx, userName, userPw); err != nil {
		return errors.Wrapf(err, "setting password for user: %s", userName)
	}

	if machineType == "laptop" {
		log.Printf("Disabling password for user %s", userName)
		if err := disablePassword(ctx); err != nil {
			return errors.Wrapf(err, "disabling password for user: %s", userName)
		}
	}

	log.Println("Creating home folders")
	if err := mkHomeFolders(ctx); err != nil {
		return errors.Wrap(err, "creating home folders")
	}

	log.Println("Configuring reflector")
	if err := configureReflector(); err != nil {
		return errors.Wrap(err, "configuring reflector")
	}

	log.Println("Enabling systemd services")
	if err := systemdServices(ctx); err != nil {
		return errors.Wrap(err, "enabling systemd services.")
	}

	log.Println("Installing AUR helper (yay)")
	if err := setupYay(ctx); err != nil {
		return errors.Wrap(err, "installing AUR helper")
	}

	log.Println("Configuring dotfiles")
	if err := configureDotFiles(ctx); err != nil {
		return errors.Wrap(err, "configuring dotfiles")
	}

	return nil
}
