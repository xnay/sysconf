package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"

	"github.com/pkg/errors"
)

const (
	dotFile2URL = "https://gitlab.com/xnay/dotfiles-arch/-/archive/main/dotfiles-arch-main.tar.gz"
	// dotFileURL     = "https://gitlab.com/xnay/dotfiles/-/archive/master/dotfiles-master.tar.gz"
	yayURL         = "https://aur.archlinux.org/cgit/aur.git/snapshot/yay.tar.gz"
	zshPath        = "/usr/bin/zsh"
	mirrorListPath = "/etc/pacman.d/mirrorlist"
)

var chroot = flag.Bool("chroot", false, "Pass this flag if you want to run inside arch-chroot.")

var machineType, userName, userPw, rootPw string

func main() {
	flag.Parse()

	log := log.New(os.Stdout, "\u001b[32m ==> \u001b[0m", log.LstdFlags|log.Ltime|log.Lshortfile)

	if err := run(context.Background(), log); err != nil {
		log.SetPrefix("\u001b[31m ==> \u001b[0m")
		log.Println("\u001b[31merror:\u001b[0m", err)
		os.Exit(1)
	}
}

func run(ctx context.Context, log *log.Logger) error {
	var err error
	var mch *machine

	if !(*chroot) {
		b, m, err := begin()
		if err != nil {
			return errors.Wrap(err, "starting installation")
		}

		if !b {
			log.Println("Aborting...")
			os.Exit(0)
		}
		mch = m
	}

	setEnvVars()

	// Installation happens in two phases. The first phase takes place on the
	// Arch USB installer. The second phase takes place in the chroot jail on
	// the mounted partition. During the second phase, this binary will be
	// copied into the chroot and run with the -chroot flag set.
	if *chroot {
		err = phase2(ctx, log)
	} else {
		err = phase1(ctx, log, mch)
	}
	if err != nil {
		return err
	}

	if !(*chroot) {
		log.Println("Installation complete!")
		log.Println("Don't forget to umount -R /mnt before restarting...")
	}

	return nil
}

func begin() (bool, *machine, error) {
	answers, err := initialPrompt()
	if err != nil {
		return false, nil, errors.Wrap(err, "prompt error: initialPrompt")
	}

	if err := answers.setEnv(); err != nil {
		return false, nil, err
	}

	begin, err := shouldBegin()
	if err != nil {
		return false, nil, errors.Wrap(err, "prompt error: shouldBegin")
	}

	return begin, NewMachine(answers), nil
}

func setEnvVars() {
	machineType = os.Getenv("MACHINE_TYPE")
	userName = os.Getenv("USERNAME")
	userPw = os.Getenv("USERPW")
	rootPw = os.Getenv("ROOTPW")
}

// hasInternet checks internet connectivity.
func hasInternet() error {
	timeOut := 3 * time.Second
	if _, err := net.DialTimeout("tcp", "google.com:80", timeOut); err != nil {
		return err
	}

	return nil
}

// bootEFI verifies the boot mode. If the command shows the directory without
// error, then the system is booted in UEFI mode. If the directory does not
// exist, the system may be booted in BIOS (or CSM) mode.
func bootEFI() (bool, error) {
	_, err := os.Stat("/sys/firmware/efi")

	if os.IsNotExist(err) {
		return false, nil
	} else if err != nil {
		return false, err
	}

	return true, nil
}

// enableNTP enables the NTP service, which sets the system clock.
func enableNTP(ctx context.Context) error {
	return Command(
		"timedatectl",
		"set-ntp",
		"true",
	).Run(ctx)
}

// updateMirrorList refreshes the mirror list.
func updateMirrorList(ctx context.Context) error {
	if err := Command(
		"reflector",
		"--verbose",
		"--latest",
		"8",
		"--protocol",
		"https",
		"--sort",
		"rate",
		"--save",
		mirrorListPath,
	).Run(ctx); err != nil {
		return err
	}

	return Command("pacman", "-Sy").Run(ctx)
}

// pacstrap runs the pacstrap command, which installs Arch Linux to the mounted
// partition.
func pacstrap(ctx context.Context) error {
	return Command(
		"pacstrap",
		"/mnt",
		"base",
		"base-devel",
		"linux",
		"linux-firmware",
		"intel-ucode",
		"archlinux-keyring",
	).Run(ctx)
}

// genfstab generates the filesystem table (fstab) for the mounted partition.
func genfstab(ctx context.Context) (err error) {
	fstab, err := os.OpenFile("/mnt/etc/fstab", os.O_WRONLY|os.O_APPEND, 0o644)
	if err != nil {
		return err
	}
	defer func() {
		if cerr := fstab.Close(); err == nil {
			err = cerr
		}
	}()

	cmd := Command(
		"genfstab",
		"-U",
		"/mnt",
	)
	cmd.RedirectStdout(fstab)

	err = cmd.Run(ctx)
	if err != nil {
		return err
	}

	return nil
}

// archChroot chroots into the mounted partition. This is the last step of phase
// 1. Phase 2 configuration takes place within the chroot jail.
func archChroot(ctx context.Context) error {
	thisBin := path.Base(os.Args[0])
	binPath := path.Join("/mnt", thisBin)
	srcPath := path.Join("/root", path.Base(os.Args[0]))

	err := Copy(binPath, srcPath)
	if err != nil {
		return err
	}

	err = Command(
		"arch-chroot",
		"/mnt",
		"/"+thisBin,
		"-chroot",
	).Run(ctx)
	if err != nil {
		return err
	}

	return os.Remove(binPath)
}

// bootloaderConfig installs the systemd-boot EFI boot manager.
func bootloaderConfig(ctx context.Context) error {
	rootLabel := os.Getenv("ROOT_UUID")
	if rootLabel == "" {
		return errors.New("empty label")
	}

	b, err := checkEFIvars()
	if err != nil {
		return errors.Wrap(err, "checking EFIvars")
	}

	if !b {
		return errors.New("not booted in EFI mode")
	}

	if err := Command(
		"bootctl",
		"--path=/boot",
		"install",
	).Run(ctx); err != nil {
		return err
	}

	f, err := os.Create("/boot/loader/loader.conf")
	if err != nil {
		return err
	}
	defer func() {
		if cerr := f.Close(); err == nil {
			err = cerr
		}
	}()

	_, err = f.WriteString(loaderConf)
	if err != nil {
		return err
	}

	f2, err := os.Create("/boot/loader/entries/arch.conf")
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if cerr := f2.Close(); err == nil {
			err = cerr
		}
	}()

	_, err = f2.WriteString(entriesConf)
	if err != nil {
		return err
	}
	_, err = f2.WriteString(fmt.Sprintf("%s", rootLabel))
	if err != nil {
		return err
	}

	return nil
}

// To install the systemd-boot EFI boot manager, first make sure the system has
// booted in UEFI mode and that UEFI variables are accessible (if the directory
// exists, the system is booted in UEFI mode).
func checkEFIvars() (bool, error) {
	_, err := os.Stat("/sys/firmware/efi/efivars")

	if os.IsNotExist(err) {
		return false, nil
	} else if err != nil {
		return false, err
	}

	return true, nil
}

func genLocale(ctx context.Context) error {
	f1, err := os.Create("/etc/locale.conf")
	if err != nil {
		return err
	}
	defer func() {
		if cerr := f1.Close(); err == nil {
			err = cerr
		}
	}()

	if _, err := f1.WriteString("LANG=en_GB.UTF-8"); err != nil {
		return err
	}

	const localeGenPath = "/etc/locale.gen"

	f2, err := os.OpenFile(localeGenPath, os.O_APPEND|os.O_WRONLY, 0o644)
	if err != nil {
		return err
	}
	defer func() {
		if cerr := f2.Close(); err == nil {
			err = cerr
		}
	}()

	if _, err := f2.WriteString("\nen_GB.UTF-8 UTF-8"); err != nil {
		return err
	}

	if _, err := f2.WriteString("\nen_GB ISO-8859-1"); err != nil {
		return err
	}

	if _, err := f2.WriteString("\nsl_SI.UTF-8 UTF-8"); err != nil {
		return err
	}

	if _, err := f2.WriteString("\nsl_SI ISO-8859-2"); err != nil {
		return err
	}

	if err := Command("locale-gen").Run(ctx); err != nil {
		return err
	}

	return nil
}

func setLocalTime(ctx context.Context) error {
	return Command(
		"ln",
		"-sf",
		"/usr/share/zoneinfo/Europe/Ljubljana",
		"/etc/localtime",
	).Run(ctx)
}

func setHWClock(ctx context.Context) error {
	return Command("hwclock", "--systohc").Run(ctx)
}

func setKeyMap() error {
	if machineType == "laptop" {
		return ioutil.WriteFile("/etc/vconsole.conf", []byte(vconsoleConfLaptop), 0o644)
	}
	return ioutil.WriteFile("/etc/vconsole.conf", []byte(vconsoleConfDesktop), 0o644)
}

func networkConfig() error {
	if machineType == "laptop" {
		if err := ioutil.WriteFile("/etc/hostname", []byte(hostnameLaptop), 0o644); err != nil {
			return err
		}

		return ioutil.WriteFile("/etc/hosts", []byte(hostsLaptop), 0o644)
	}

	if err := ioutil.WriteFile("/etc/hostname", []byte(hostnameDesktop), 0o644); err != nil {
		return err
	}

	return ioutil.WriteFile("/etc/hosts", []byte(hostsDesktop), 0o644)
}

func setPassword(ctx context.Context, username, pwd string) error {
	if username == "" {
		return errors.New("no username given")
	}

	if pwd == "" {
		return errors.New("empty password")
	}

	cmd := Command("chpasswd", username)
	cmd.Stdin = strings.NewReader(fmt.Sprintf("%s:%s", username, pwd))

	return cmd.Run(ctx)
}

func disablePassword(ctx context.Context) error {
	return Command("passwd", "-d", userName).Run(ctx)
}

func installPkgs(ctx context.Context) error {
	cmd := Command("pacman", common...)
	cmd.Stdin = strings.NewReader("1\n")
	if err := cmd.Run(ctx); err != nil {
		return err
	}

	if machineType == "desktop" {
		cmd := Command("pacman", desktop...)
		cmd.Stdin = strings.NewReader("1\n")
		if err := cmd.Run(ctx); err != nil {
			return err
		}
	}

	return nil
}

func updateSudoers() error {
	return ioutil.WriteFile("/etc/sudoers", []byte(sudoers), 0o440)
}

func createUser(ctx context.Context, defaultShell string) error {
	if err := Command("groupadd", userName).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"useradd",
		"-m",
		"-g",
		userName,
		"-G",
		"wheel",
		"-s",
		defaultShell,
		userName,
	).Run(ctx); err != nil {
		return err
	}

	if machineType == "desktop" {
		if err := Command(
			"usermod",
			"-aG",
			"docker",
			userName,
		).Run(ctx); err != nil {
			return err
		}
	}

	return nil
}

func configureReflector() error {
	return ioutil.WriteFile("/etc/xdg/reflector/reflector.conf", []byte(reflectorConf), 0o644)
}

func systemdServices(ctx context.Context) error {
	if err := Command(
		"systemctl",
		"enable",
		"fstrim.timer",
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"systemctl",
		"enable",
		"reflector.timer",
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"systemctl",
		"enable",
		"NetworkManager.service",
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"systemctl",
		"enable",
		"rngd.service",
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"systemctl",
		"enable",
		"lightdm",
	).Run(ctx); err != nil {
		return err
	}

	return Command(
		"systemctl",
		"enable",
		"paccache.timer",
	).Run(ctx)
}

func configureDotFiles(ctx context.Context) error {
	if err := os.Chdir("/tmp"); err != nil {
		return err
	}

	if err := Command(
		"sudo",
		"-u",
		userName,
		"wget",
		dotFile2URL,
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"sudo",
		"-u",
		userName,
		"tar",
		"-xzf",
		"dotfiles-arch-main.tar.gz",
	).Run(ctx); err != nil {
		return err
	}

	homeDir := filepath.Join("/home", userName)

	return Command(
		"sudo",
		"-u",
		userName,
		"rsync",
		"-a",
		"dotfiles-arch-main/",
		homeDir,
	).Run(ctx)
}

func mkHomeFolders(ctx context.Context) error {
	p1 := path.Join("/home", userName, "Desktop")
	if err := Command("sudo", "-u", userName, "mkdir", "-p", p1).Run(ctx); err != nil {
		return err
	}

	p2 := path.Join("/home", userName, "Downloads")
	if err := Command("sudo", "-u", userName, "mkdir", "-p", p2).Run(ctx); err != nil {
		return err
	}

	p3 := path.Join("/home", userName, "Workspace")
	return Command("sudo", "-u", userName, "mkdir", "-p", p3).Run(ctx)

}

func setupYay(ctx context.Context) error {
	if err := os.Chdir("/tmp"); err != nil {
		return err
	}

	if err := Command(
		"sudo",
		"-u",
		userName,
		"wget",
		yayURL,
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"sudo",
		"-u",
		userName,
		"tar",
		"-xzf",
		"yay.tar.gz",
	).Run(ctx); err != nil {
		return err
	}

	if err := Command(
		"chown",
		"-R",
		userName,
		"/tmp/yay",
	).Run(ctx); err != nil {
		return err
	}

	if err := os.Chdir("yay/"); err != nil {
		return err
	}

	return Command(
		"sudo",
		"-u",
		userName,
		"makepkg",
		"--noconfirm",
		"-si",
	).Run(ctx)
}
