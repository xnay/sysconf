package main

import (
	"github.com/AlecAivazis/survey/v2"
	"os"
)

type Answers struct {
	MachineType  string
	UserName     string
	Password     string
	RootPassword string
}

func initialPrompt() (*Answers, error) {
	var as Answers

	qs := []*survey.Question{
		{
			Name: "MachineType",
			Prompt: &survey.Select{
				Message: "Choose a machine type:",
				Options: []string{"desktop", "laptop"},
				Default: "desktop",
			},
		},
		{
			Name:      "UserName",
			Prompt:    &survey.Input{Message: "Username:"},
			Validate:  survey.Required,
			Transform: survey.ToLower,
		},
		{
			Name:      "Password",
			Prompt:    &survey.Input{Message: "Password:"},
			Validate:  survey.Required,
			Transform: survey.ToLower,
		},

		{
			Name:      "RootPassword",
			Prompt:    &survey.Input{Message: "Root password:"},
			Validate:  survey.Required,
			Transform: survey.ToLower,
		},
	}

	err := survey.Ask(qs, &as)
	if err != nil {
		return nil, err
	}

	return &as, nil
}

func (as *Answers) setEnv() error {
	if err := os.Setenv("MACHINE_TYPE", as.MachineType); err != nil {
		return err
	}

	if err := os.Setenv("USERNAME", as.UserName); err != nil {
		return err
	}

	if err := os.Setenv("USERPW", as.Password); err != nil {
		return err
	}

	if err := os.Setenv("ROOTPW", as.RootPassword); err != nil {
		return err
	}

	return nil
}

func shouldBegin() (bool, error) {
	begin := false
	shouldBegin := &survey.Confirm{
		Message: " Proceed with installation?",
	}

	if err := survey.AskOne(shouldBegin, &begin); err != nil {
		return false, err
	}

	return begin, nil
}
