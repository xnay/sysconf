SHELL := /usr/bin/env bash

all: bin/sysconf

.PHONY: bin.sysconf
bin/sysconf:
# https://docs.docker.com/engine/reference/commandline/build/#custom-build-outputs
# https://www.docker.com/blog/containerize-your-go-developer-environment-part-1/
	@docker build . \
		--file sysconf.dockerfile \
		--target bin-unix \
		--output bin/ \
		--platform local \
		&& docker system prune -f \
		&& unset DOCKER_BUILDKIT

.PHONY: deps-reset
deps-reset:
	git checkout -- go.mod
	go mod tidy
	go mod vendor

.PHONY: tidy
tidy:
	go mod tidy
	go mod vendor

.PHONY: deps-upgrade
deps-upgrade:
	# go get $(go list -f '{{if not (or .Main .Indirect)}}{{.Path}}{{end}}' -m all)
	go get -u -t -d -v ./...
	go mod tidy
	go mod vendor

.PHONY: deps-cleancache
deps-cleancache:
	go clean -modcache

