package main

import (
	"context"
	"log"
	"time"

	"github.com/pkg/errors"
)

func phase1(ctx context.Context, log *log.Logger, m *machine) error {
	log.Println("Checking internet connectivity")
	if err := hasInternet(); err != nil {
		return errors.Wrap(err, "internet isn't working")
	}

	log.Println("Checking boot mode")
	b, err := bootEFI()
	if err != nil {
		return errors.Wrap(err, "checking boot mode")
	}
	if !b {
		return errors.New("not booted in EFI mode")
	}

	log.Println("Enabling NTP")
	if err := enableNTP(ctx); err != nil {
		return errors.Wrap(err, "enabling NTP")
	}

	errc := make(chan error, 1)
	go func() {
		log.Println("Refreshing mirrors")
		errc <- updateMirrorList(ctx)
	}()

	log.Printf("Wiping target disk %s", m.Disk["targetDisk"])
	if err := m.wipeDisk(ctx); err != nil {
		return errors.Wrapf(err, "wiping disk: %s", m.Disk["targetDisk"])
	}

	log.Printf("Partitioning target disk %s", m.Disk["targetDisk"])
	if err := m.partitionDisk(ctx); err != nil {
		return errors.Wrapf(err, "partitioning disk: %s", m.Disk["targetDisk"])
	}

	log.Println("Formatting partitions")
	if err := m.formatPartitions(ctx); err != nil {
		return errors.Wrapf(err, "formatting partitions")
	}

	log.Println("Mounting partitions")
	if err := m.mountPartitions(ctx); err != nil {
		return errors.Wrapf(err, "mounting partitions")
	}

	log.Println("Setting root UUID")
	if err := m.setRootUUID(ctx); err != nil {
		return errors.Wrap(err, "setting root UUID")
	}

	log.Println("Waiting for updateMirrorList() to complete before running pacstrap")
	if err = <-errc; err != nil {
		return errors.Wrap(err, "waiting for updateMirrorList()")
	}

	log.Println("Running pacstrap")
	if err := pacstrap(ctx); err != nil {
		return errors.Wrap(err, "running pacstrap")
	}

	log.Println("Generating fstab")
	if err := genfstab(ctx); err != nil {
		return errors.Wrap(err, "generating fstab")
	}

	log.Println("Phase 1 complete, entering chroot environment...")
	time.Sleep(3 * time.Second)

	return archChroot(ctx)
}
